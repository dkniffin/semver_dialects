require 'rspec'
require_relative '../../lib/utils.rb'

RSpec.describe String do
  context 'String utility functions' do
    it 'unquote should work properly' do
      expect('\'Hello"'.unquote == 'Hello')
      expect('"Hello"'.unquote == 'Hello')
      expect('\'Hello\''.unquote == 'Hello')
      expect('"Hello\''.unquote == 'Hello')
      expect('Hello"'.unquote == 'Hello')
    end

    it 'CSV unquote should work properly' do
      expect('"""Hello"""'.csv_unquote == 'Hello')
    end

    it 'remove trailing number should work' do
      expect('"""Hello"""'.csv_unquote == 'Hello')
    end

    it 'chars_only to extract printable chars only should work' do
      expect('hello123:\-;*#@test'.chars_only == 'hello123test')
    end
  end
end
