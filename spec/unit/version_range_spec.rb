# frozen_string_literal: true

require 'rspec'
require_relative '../../lib/semver_dialects/semantic_version/version_range.rb'
require_relative '../../lib/semver_dialects/semantic_version/version_parser.rb'
require_relative '../../lib/semver_dialects/semantic_version/version_translator.rb'

RSpec.describe VersionRange do
  context 'Semantic Version comparison' do
    it 'should work for a variety of different version strings' do
      version0 = VersionParser.parse('<=1.6.16')
      version1 = VersionParser.parse('=2.0')
      version2 = VersionParser.parse('=2.0.0')
      version3 = VersionParser.parse('=2.0.1')
      version4 = VersionParser.parse('=2.0.2')

      range = VersionRange.new
      range.add(version0)
      range.add(version1)
      range.add(version2)
      range.add(version3)
      range.add(version4)

      expect(range.size == 3)
      expect(range.version_intervals[0] == version0)
      expect(range.version_intervals[1] == version1)
      expect(range.version_intervals[2] == VersionParser.parse('>=2.0.0 <=2.0.2'))
    end

    it 'should work for pypi' do
      version_string = '>=2.7.0dev0,<2.7.1 || >=2.6.0dev0,<2.6.7 || <2.5.11'
      version_array = VersionTranslator.translate_pypi(version_string)
      vr = VersionRange.new(false)
      version_array.each do |item|
        interval = VersionParser.parse(item)
        vr.add(interval)
      end
      expect(version_array.size == 3)
    end

    it 'should work for maven' do
      version_string = '[8.5.0, 8.5.32), [9.0.0.M1, 9.0.10)'
      version_array = VersionTranslator.translate_maven(version_string)
      vr = VersionRange.new(false)
      version_array.each do |item|
        interval = VersionParser.parse(item)
        vr.add(interval)
      end
      expect(version_array.size == 2)

      version_string = '(,3.1.13],3.2.0,[,]'
      version_array = VersionTranslator.translate_maven(version_string)
      vr = VersionRange.new(false)
      version_array.each do |item|
        interval = VersionParser.parse(item)
        vr.add(interval)
      end
      expect(version_array.size == 3)
    end

    it 'should work for nuget' do
      version_string = '[8.5.0, 8.5.32), [9.0.0.M1, 9.0.10)'
      version_array = VersionTranslator.translate_nuget(version_string)
      vr = VersionRange.new(false)
      version_array.each do |item|
        interval = VersionParser.parse(item)
        vr.add(interval)
      end
      expect(version_array.size == 2)

      version_string = '(,3.1.13],3.2.0,[,]'
      version_array = VersionTranslator.translate_nuget(version_string)
      vr = VersionRange.new(false)
      version_array.each do |item|
        interval = VersionParser.parse(item)
        vr.add(interval)
      end
      expect(version_array.size == 3)
    end

    it 'should work for packagist' do
      version_string = '5.2.18||5.2.19'
      version_array = VersionTranslator.translate_packagist(version_string)
      vr = VersionRange.new(false)
      version_array.each do |item|
        interval = VersionParser.parse(item)
        vr.add(interval)
      end
      expect(version_array.size == 2)
    end

    it 'should represent univeral ranges correcty' do
      version_array = ['=*']
      vr = VersionRange.new(false)
      version_array.each do |item|
        interval = VersionParser.parse(item)
        vr.add(interval)
      end
      expect vr.universal?

      version_string = '>= 0.0.0'
      version_array = VersionTranslator.translate_packagist(version_string)
      vr = VersionRange.new(false)
      version_array.each do |item|
        interval = VersionParser.parse(item)
        vr.add(interval)
      end
      expect vr.universal?
    end

    it 'should be able to invert versions correctly' do
      # range: (2,10],(12,13],[15,+inf)
      # invert: (-inf,2],(10,+inf),(-inf,12],(13,+inf),(-inf,15)
      # collapse: (-inf,2],(10,12],(13,15)
      version0 = VersionParser.parse('>2 <=10')
      version1 = VersionParser.parse('>12 <=13')
      version2 = VersionParser.parse('>=15')
      range = VersionRange.new
      range.add(version0)
      range.add(version1)
      range.add(version2)
      inversion = range.invert.version_intervals
      expect(inversion.size == 3)
      expect(inversion[0] == VersionParser.parse('<=2'))
      expect(inversion[1] == VersionParser.parse('>10 <=12'))
      expect(inversion[2] == VersionParser.parse('>13 <15'))
    end

    it 'should be able to join versions correctly' do
      version0 = VersionParser.parse('>2')
      version1 = VersionParser.parse('<=10')
      range = VersionRange.new
      range.add(version0)
      range.add(version1)
      range2 = range.collapse
      expect(range2.version_intervals.first == VersionParser.parse('>2 <=10'))
      expect(range.size == 2)
    end

    it 'should be able to join unbounded versions correctly' do
      version0 = VersionParser.parse('=*')
      version1 = VersionParser.parse('=*')
      version2 = VersionParser.parse('<=3.23.0')
      range = VersionRange.new
      range.add(version0)
      range.add(version1)
      range.add(version2)
      range2 = range.collapse
      expect(range2.version_intervals.first == VersionParser.parse('<=3.23.0'))
      expect(range.size == 1)
    end
  end
end
