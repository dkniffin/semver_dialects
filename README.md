# `semver_dialects`, a generic semantic version matching gem

`semver_dialects` is a gem for generic semantic version matcher that supports
different semantic version dialects:
- pypi
- composer/packagist
- maven
- gem
- go
- npm
- nuget
- conan

`semver_dialects` ...
- provides a unified interface to the language specific dialects.
- matches semantic versions in a language agnostic way.
- is able to collapse a list of concrete version into version ranges.
- invert ranges.
- can cope with scattered, non-consecutive ranges.
- can parse and produce different version syntaxes.
- matches version gracefully and handles input versions/constraints in a best-effort manner.


You can find a detailed description of the theoretical foundation we used for `semver_dialects` in this [blog post](https://about.gitlab.com/blog/2021/09/28/generic-semantic-version-processing/)

## Installation

`semver_dialects` is available on [rubygems.org](https://rubygems.org/gems/semver_dialects). 
You can install it by adding the following line to your application's Gemfile:

```ruby
gem 'semver_dialects'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install semver_dialects

## Usage and Approach

### Semantic Version Representation

A semantic version string is composed of prefix and suffix where the prefix
contains the following segments.

``` ruby
s1 = SemanticVersion.new('1.2.3')
puts "segments: #{s1}"
# segments: 1:2:3
puts "major #{s1.major}"
# major 1
puts "minor #{s1.minor}"
# minor 2
puts "patch #{s1.patch}"
# patch 3
```
SemverDialects translates language specific version suffixes into numeric
values `2.0.0.RC1` ⇒ `2:-11:1` (e.g., `RC` is mapped to `-11`).  Semver
matching can be achieved by iterating through the segments and numerically
comparing them. For unknown suffices, use lexical matching (default fallback).

Comparing two semantic versions is a two-step process:

1. Extend both semantic version to have the same prefix length and suffix
lengths by appending zeros.
2. Iterate over segments and compare them numerically.

### Constraint Syntax - Everything is a Range: Linear Interval Arithmetic

For representing version constraints, `semver_dialects` uses 
[linear intervals](https://www.math.kit.edu/ianm2/~kulisch/media/arjpkx.pdf).

``` ruby
puts VersionParser.parse(">=1, <=2")
# [1,2]
puts VersionParser.parse(">1, <=2")
# (1,2]
puts VersionParser.parse(">=1, <2")
# [1,2)
puts VersionParser.parse(">1, <2")
# (1,2)
puts VersionParser.parse("<=2")
# (-inf,2]
puts VersionParser.parse(">=1")
# [1,+inf)
```

For solving constraints, `semver_dialects` leverages 
[interval arithmetic](https://www.math.kit.edu/ianm2/~kulisch/media/arjpkx.pdf).

#### Intersection

Interval intersection can be used to compute the overlap between of two intervals.

``` ruby
puts VersionParser.parse(">=2, <=5").intersect(VersionParser.parse(">=3, <=10"))
# [3,5]
puts VersionParser.parse(">=2, <=5").intersect(VersionParser.parse(">=7, <=10"))
# empty
```

#### Union 

Interval union can be used to combine intervals.

``` ruby
puts "union: #{VersionParser.parse(">=2, <=5").union(VersionParser.parse(">=3, <=10"))}"
# union: [2,10]
r1 = VersionRange.new
r1.add(VersionParser.parse(">=2, <=5")); r1.add(VersionParser.parse(">=3, <=10"))
puts "r1: #{r1}"
# union: [2,10]
puts "r1 collapsed: #{r1.collapse}" # creates the union between intervals
# r1 collapsed: [2,10]
r2 = VersionRange.new
r2.add(VersionParser.parse(">=2, <=5")); r2.add(VersionParser.parse(">=7, <=10"))
puts "r2: #{r2}"
# r2: [2,5],[7,10]
```

#### Complement

Complement can be used to invert ranges.

``` ruby
r1 = VersionRange.new; r1.add(VersionParser.parse(">=1, <=3"))
puts r1.invert
#(-inf,1),(3,+inf)

r2 = VersionRange.new
r2.add(VersionParser.parse(">=2.1.2, <=5.1.2")); r2.add(VersionParser.parse(">3.1, <10"))
puts r2.collapse.invert
# (-inf,2.1.2),[10,+inf)
``` 

#### Version Matching

The examples below illustrate how you can verify whether a version falls into a
specific range. 

``` ruby
r1 = VersionRange.new
r1.add(VersionParser.parse(">=2.1.2, <=5.1.2")); r1.add(VersionParser.parse(">3.1, <10"))

puts "[0,2.1) in #{r1}? #{r1.overlaps_with?(VersionParser.parse(">=0, <2.1"))}"
# [0,2.1) in [2.1.2,5.1.2],(3.1,10)? false
puts "[5.5,5.5] overlap with #{r1}? #{r1.overlaps_with?(VersionParser.parse("=5.5"))}"
# [5.5,5.5] overlap with [2.1.2,5.1.2],(3.1,10)? true
```

### Version Syntaces 

`semver_dialects` supports different native version syntaces:
- `gem`: [gem requirement](https://guides.rubygems.org/specification-reference/#add_runtime_dependency): `>=2.0.0 <=2.12.4||>=3.0.0 <=4.0.0.beta7`
- `maven`: [Maven Dependency Version Requirement Specification](https://maven.apache.org/pom.html#Dependency_Version_Requirement_Specification): `[1.0-alpha0,1.0.3.RELEASE],[1.1-alpha0,1.1.2.RELEASE)`
- `npm`: [node-semver](https://github.com/npm/node-semver#ranges): `<1.6.5 || <2.1.7 >=2.0.0-beta0`
- `php`: [PHP Composer version constraints](https://getcomposer.org/doc/articles/versions.md#writing-version-constraints): `<2.2.8||>=2.3.0-alpha0,<2.3.3`
- `pypi`: [PEP440](https://www.python.org/dev/peps/pep-0440/#version-specifiers): `>=3.0.0,<=3.0.17||>=3.2.0,<=3.2.17||>=3.4.0,<=3.4.17`
- `go`: [go semver](https://godoc.org/golang.org/x/tools/internal/semver): `>=1.11.0 <1.11.9||>=1.12.0 <1.12.7||>=1.13.0 <1.13.5||=1.14.0`


#### Version Translation

For translating native version sytaces to the generic `semver_dialects`
representation, you can use the `VersionTranslator` module.

``` ruby
vs_packagist = "<2.5.9||>=2.6.0,<2.6.11"
vs_maven = "(,2.5.9),[2.6.0,2.6.11)"

puts VersionTranslator.translate_packagist(vs_packagist).to_s
# ["<2.5.9", ">=2.6.0 <2.6.11"]
puts VersionTranslator.translate_maven(vs_maven).to_s
# ["<2.5.9", ">=2.6.0 <2.6.11"]
```

## Copyright

Copyright (c) 2020 GitLab Inc. See [MIT License](LICENSE.txt) for further details.
